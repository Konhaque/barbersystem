package com.projetobeta.barbersystem.funcionalidades.clientes;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.projetobeta.barbersystem.R;
import com.projetobeta.barbersystem.bd.Cliente;
import com.projetobeta.barbersystem.bd.Repository;
import com.projetobeta.barbersystem.general.AbreTela;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Cadastrar_Cliente extends Fragment {
    private EditText nome;
    private EditText dataNascimento;
    private EditText cep;
    private EditText logradouro;
    private EditText bairro;
    private EditText cpf;
    private EditText cidade;
    private Button salvar;
    private AlertDialog dialog;
    private Toolbar toolbar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.cadastrar_cliente,container,false);
    }

    @Override
    public void onStart() {
        iniciarObjetos();
        setDataNascimento();
        setCep();
        setSalvar();
        setToolbar();
        super.onStart();
    }

    private void iniciarObjetos(){
        nome = (EditText) getActivity().findViewById(R.id.nome);
        dataNascimento = (EditText) getActivity().findViewById(R.id.data_nascimento);
        cep = (EditText) getActivity().findViewById(R.id.cep);
        logradouro = (EditText) getActivity().findViewById(R.id.logradouro);
        bairro = (EditText) getActivity().findViewById(R.id.bairro);
        cidade = (EditText) getActivity().findViewById(R.id.cidade);
        cpf = (EditText) getActivity().findViewById(R.id.cpf);
        salvar = (Button) getActivity().findViewById(R.id.salvarCliente);
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbarCadastrarCliente);
    }

    private void setToolbar(){
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                new AbreTela(getActivity().getSupportFragmentManager(),new Clientes(),R.id.setTelaClientes);
                return true;
            default: return super.onOptionsItemSelected(item);
        }
    }

    private void setDataNascimento(){
        Calendar calendar = Calendar.getInstance();
        int ano = calendar.get(Calendar.YEAR);
        int mes = calendar.get(Calendar.MONTH);
        int dia = calendar.get(Calendar.DAY_OF_MONTH);
        final Dialog dialog = new DatePickerDialog(getContext(), android.R.style.Theme_Holo_Light_Dialog_MinWidth, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                dataNascimento.setText(i2+"/"+(i1+1)+"/"+i);
            }
        },ano,mes,dia);
        dataNascimento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dataNascimento.requestFocus();
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });
    }

    private void setCep(){
        cep.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(cep.length() == 8) new BuscaCep().execute();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void setSalvar(){
        salvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                LayoutInflater inflater = getLayoutInflater();
                builder.setView(inflater.inflate(R.layout.activity_main,null));
                builder.setCancelable(true);
                dialog = builder.create();
                dialog.show();
                verificar();
            }
        });
    }


    private void verificar(){
            if(nome.getText().length() == 0 || nome.getText().toString().equals(" ")) setError(nome);
            else if (dataNascimento.getText().length() == 0 || dataNascimento.getText().toString().equals(" ")) setError(dataNascimento);
            else if(cep.getText().length() == 0 || cep.getText().toString().equals(" ")) setError(cep);
            else if(cpf.getText().length() == 0) setError(cpf);
            else if(logradouro.getText().length() == 0 || logradouro.getText().toString().equals(" ")) setError(logradouro);
            else if(bairro.getText().length() == 0 || bairro.getText().toString().equals(" ")) setError(bairro);
            else if(cidade.getText().length() == 0 || bairro.getText().toString().equals(" ")) setError(cidade);
            else salvar();
    }

    private void salvar(){
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Clientes");
        databaseReference.child(databaseReference.push().getKey()).setValue(carregarCliente()).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                alertaSucesso();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                e.getMessage();
            }
        });

    }

    private Cliente carregarCliente(){
        Cliente cliente = new Cliente();
        cliente.setIdUsuario(new Repository(getContext()).getUsuario());
        cliente.setNome(nome.getText().toString());
        cliente.setCpf(cpf.getText().toString());
        cliente.setDataNascimento(dataNascimento.getText().toString());
        cliente.setCep(cep.getText().toString());
        cliente.setLogradouro(logradouro.getText().toString());
        cliente.setBairro(bairro.getText().toString());
        cliente.setCidade(cidade.getText().toString());
        return cliente;
    }

    private void setError(EditText editText) {
        dialog.dismiss();
        editText.setError(getString(R.string.campo_sem_preencher));
        editText.requestFocus();
    }

    private void alertaSucesso(){
        dialog.dismiss();
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.alerta_sucesso,null);
        TextView textView = view.findViewById(R.id.textAlerta);
        ImageView imageView = view.findViewById(R.id.imgAlerta);
        Button sair = view.findViewById(R.id.sair);
        sair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nome.getText().clear();
                dataNascimento.getText().clear();
                cep.getText().clear();
                logradouro.getText().clear();
                bairro.getText().clear();
                cidade.getText().clear();
                dialog.dismiss();
            }
        });
        textView.setText("Cliente salvo com sucesso!");
        imageView.setImageDrawable(getActivity().getDrawable(R.drawable.rosto_1));
        builder.setView(view);
        builder.setCancelable(true);
        dialog = builder.create();
        dialog.show();
    }




    private class BuscaCep extends AsyncTask<Void,Void,Void>{
        private JSONObject jsonObject;

        @Override
        protected void onPreExecute() {
            carregar();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            dialog.dismiss();
            setInformacoes();
            super.onPostExecute(aVoid);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            cep();
            return null;
        }

        private void cep(){
            try{
                OkHttpClient client = new OkHttpClient.Builder().build();
                Request request = new Request.Builder().url("http://viacep.com.br/ws/"+cep.getText().toString()+"/json/")
                        .method("GET",null)
                        .build();
                Response response = client.newCall(request).execute();
                String as = response.body().string();
                try {
                    jsonObject = new JSONObject(as);
                }catch (JSONException e){
                    e.getMessage();
                }
            }catch (IOException e){
                e.getMessage();
            }
        }
        private void carregar(){
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            LayoutInflater inflater = getLayoutInflater();
            builder.setView(inflater.inflate(R.layout.activity_main,null));
            builder.setCancelable(true);
            dialog = builder.create();
            dialog.show();
        }
        private void setInformacoes(){
            try {
                logradouro.setText(jsonObject.getString("logradouro"));
                bairro.setText(jsonObject.getString("bairro"));
                cidade.setText(jsonObject.getString("localidade"));
            }catch (JSONException e){
                e.getMessage();
            }
        }
    }




}
