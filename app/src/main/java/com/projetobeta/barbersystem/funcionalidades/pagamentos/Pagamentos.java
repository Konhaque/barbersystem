package com.projetobeta.barbersystem.funcionalidades.pagamentos;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.projetobeta.barbersystem.R;
import com.projetobeta.barbersystem.general.AbreTela;

public class Pagamentos extends Fragment {

    private Toolbar toolbar;
    private FrameLayout adicionarPagamento;
    private FrameLayout adicionarPromocao;
    private FrameLayout adicionarDespesa;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.pagamentos,container,false);
    }

    @Override
    public void onStart() {
        iniciarObjetos();
        setToolbar();
        setAdicionarDespesa();
        setAdicionarPromocao();
        setAdicionarPagamento();
        super.onStart();
    }

    private void iniciarObjetos(){
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbarPagamentos);
        adicionarPagamento = (FrameLayout) getActivity().findViewById(R.id.lancamento);
        adicionarPromocao = (FrameLayout) getActivity().findViewById(R.id.promocao);
        adicionarDespesa = (FrameLayout) getActivity().findViewById(R.id.despesa);
    }

    private void setToolbar(){
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                getActivity().finish();
                return true;
            default: return super.onOptionsItemSelected(item);
        }
    }

    private void  setAdicionarDespesa(){
        adicionarDespesa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AbreTela(getActivity().getSupportFragmentManager(),new Adicionar_Despesa(),R.id.setTelaPagamentos);
            }
        });
    }
    private void setAdicionarPromocao(){
        adicionarPromocao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AbreTela(getActivity().getSupportFragmentManager(),new Promocao(),R.id.setTelaPagamentos);
            }
        });
    }

    private void setAdicionarPagamento(){
        adicionarPagamento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AbreTela(getActivity().getSupportFragmentManager(),new Adicionar_Lancamento(),R.id.setTelaPagamentos);
            }
        });
    }

}
