package com.projetobeta.barbersystem.funcionalidades.registro;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.projetobeta.barbersystem.R;
import com.projetobeta.barbersystem.bd.Repository;
import com.projetobeta.barbersystem.bd.Servicos;
import com.projetobeta.barbersystem.general.AbreTela;

public class Cadastrar_Servico extends Fragment {

    private EditText servico;
    private EditText valor;
    //private TextView addProdutos;
    private LinearLayout produtos;
    private Button salvar;
    private Toolbar toolbar;
    private AlertDialog dialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.cadastrar_servico,container,false);
    }

    @Override
    public void onStart() {
        iniciar_Objetos();
        setToolbar();
        setSalvar();
        //setAddProdutos();
        super.onStart();
    }

    private void iniciar_Objetos(){
        servico = (EditText) getActivity().findViewById(R.id.servico);
        valor = (EditText) getActivity().findViewById(R.id.preco);
        //addProdutos = (TextView) getActivity().findViewById(R.id.adicionarProdutos);
        produtos = (LinearLayout) getActivity().findViewById(R.id.produtos);
        salvar = (Button) getActivity().findViewById(R.id.salvar);
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar_cs);
    }


    private void setToolbar(){
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                new AbreTela(getActivity().getSupportFragmentManager(),new Registro(),R.id.setTelaRegistros);
                return true;
            default: return super.onOptionsItemSelected(item);
        }
    }

    private void setSalvar(){
        salvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                LayoutInflater inflater = getLayoutInflater();
                builder.setView(inflater.inflate(R.layout.activity_main,null));
                builder.setCancelable(true);
                dialog = builder.create();
                dialog.show();
                validar();
            }
        });
    }

    private void validar(){
        if(servico.getText().length() == 0 || servico.getText().toString().equals(" ")) setError(servico);
        else if (valor.getText().length() == 0) setError(valor);
        else salvar();
    }

    private void setError(EditText editText){
        editText.setError(getString(R.string.campo_sem_preencher));
        editText.requestFocus();
        dialog.dismiss();
    }

    private void salvar(){
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Servicos");
        databaseReference.child(databaseReference.push().getKey()).setValue(carregarServico()).
                addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                dialog.dismiss();
                alertaSucesso();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });
    }

    private Servicos carregarServico(){
        Servicos servicos = new Servicos();
        servicos.setIdUsuario(new Repository(getContext()).getUsuario());
        servicos.setServico(servico.getText().toString());
        servicos.setValor(Double.parseDouble(valor.getText().toString()));
        return servicos;
    }

    private void alertaSucesso(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.alerta_sucesso, null);
        TextView textView = view.findViewById(R.id.textAlerta);
        ImageView imageView = view.findViewById(R.id.imgAlerta);
        Button sair = view.findViewById(R.id.sair);
        sair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                servico.getText().clear();
                valor.getText().clear();
                dialog.dismiss();
            }
        });
        textView.setText("Serviço salvo com sucesso!");
        imageView.setImageDrawable(getActivity().getDrawable(R.drawable.rosto_2));
        builder.setView(view);
        builder.setCancelable(true);
        dialog = builder.create();
        dialog.show();
    }


    /*private void setAddProdutos(){
        addProdutos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                LayoutInflater inflater = getLayoutInflater();
                View vv = inflater.inflate(R.layout.lista_produtos,null);
                LinearLayout linearLayout = vv.findViewById(R.id.lista_produtos);
                builder.setView(vv);
                builder.setCancelable(true);
                getProdutos(linearLayout, builder);
            }
        });
    }

    private void getProdutos(final LinearLayout linearLayout, final AlertDialog.Builder builder){
        DatabaseReference db = FirebaseDatabase.getInstance().getReference("Produtos");
        db.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot ds : snapshot.getChildren()){
                    setProdutos(ds,linearLayout);
                }
                dialog = builder.create();
                dialog.show();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


    }

    private void setProdutos(DataSnapshot snapshot, LinearLayout linearLayout){
        FrameLayout layout = new FrameLayout(getContext());
        layout.setBackground(getActivity().getDrawable(R.drawable.container));
        layout.setPadding(30,20,30,20);
        TextView textView = new TextView(getContext());
        textView.setText(snapshot.child("produto").getValue().toString());
        textView.setTextColor(Color.BLACK);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        layout.addView(textView);
        linearLayout.addView(layout);

    }*/


}
