package com.projetobeta.barbersystem.funcionalidades.pagamentos;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.projetobeta.barbersystem.R;
import com.projetobeta.barbersystem.bd.Despesa;
import com.projetobeta.barbersystem.bd.Repository;
import com.projetobeta.barbersystem.general.AbreTela;

import java.util.ArrayList;

public class Adicionar_Despesa extends Fragment {

    private Toolbar toolbar;
    private EditText descricao;
    private EditText valor;
    private Spinner tipo;
    private Button salvar;
    private AlertDialog dialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.adicionar_despesa,container,false);
    }

    @Override
    public void onStart() {
        iniciarObjetos();
        setToolbar();
        setTipo();
        setSalvar();
        super.onStart();
    }

    private void iniciarObjetos(){
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbarDespesas);
        descricao = (EditText) getActivity().findViewById(R.id.descricao);
        valor = (EditText) getActivity().findViewById(R.id.valor);
        tipo = (Spinner) getActivity().findViewById(R.id.tipoDespesa);
        salvar = (Button) getActivity().findViewById(R.id.cadastrarDespesa);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                new AbreTela(getActivity().getSupportFragmentManager(),new Pagamentos(),R.id.setTelaPagamentos);
                return true;
            default: return super.onOptionsItemSelected(item);
        }
    }

    private void setToolbar(){
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);
    }

    private void setTipo(){
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(),R.layout.spinner_item,carregarTipo());
        arrayAdapter.setDropDownViewResource(R.layout.spinner_dropdown);
        tipo.setAdapter(arrayAdapter);
    }

    private ArrayList<String> carregarTipo(){
        ArrayList<String> tipos = new ArrayList<>();
        tipos.add("Impostos");
        tipos.add("Produtos");
        tipos.add("Funcionários");
        tipos.add("Outras Despesas");
        return tipos;
    }

    private void setSalvar(){
        salvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                LayoutInflater inflater = getLayoutInflater();
                builder.setView(inflater.inflate(R.layout.activity_main,null));
                builder.setCancelable(true);
                dialog = builder.create();
                dialog.show();
                verificar();
            }
        });
    }


    private void verificar(){
        if(descricao.getText().length() == 0 || descricao.getText().toString().equals(" ")) setError(descricao);
        else if(valor.getText().length() == 0 || valor.getText().toString().equals(" ")) setError(valor);
        else salvar();
    }

    private void salvar(){
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Despesas");
        databaseReference.child(databaseReference.push().getKey()).setValue(carregarDespesa()).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                alertaSucesso();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });

    }


    private void setError(EditText editText){
        editText.setError(getString(R.string.campo_sem_preencher));
        editText.requestFocus();
        dialog.dismiss();
    }

    private Despesa carregarDespesa(){
        Despesa despesa = new Despesa();
        despesa.setIdUsuario(new Repository(getContext()).getUsuario());
        despesa.setDescricao(descricao.getText().toString());
        despesa.setTipo(tipo.getSelectedItem().toString());
        despesa.setValor(Double.parseDouble(valor.getText().toString()));
        return despesa;
    }

    private void alertaSucesso(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.alerta_sucesso,null);
        ImageView imageView = view.findViewById(R.id.imgAlerta);
        TextView textView = view.findViewById(R.id.textAlerta);
        Button button = view.findViewById(R.id.sair);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                descricao.getText().clear();
                valor.getText().clear();
                dialog.dismiss();
            }
        });
        textView.setText("Despesa cadastrada com sucesso!");
        imageView.setImageDrawable(getActivity().getDrawable(R.drawable.dinheiro_branco));
        builder.setView(view);
        builder.setCancelable(true);
        dialog = builder.create();
        dialog.show();
    }


}
