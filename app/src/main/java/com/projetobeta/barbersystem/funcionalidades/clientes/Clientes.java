package com.projetobeta.barbersystem.funcionalidades.clientes;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.projetobeta.barbersystem.R;
import com.projetobeta.barbersystem.funcionalidades.clientes.Cadastrar_Cliente;
import com.projetobeta.barbersystem.funcionalidades.clientes.Lista_Clientes;
import com.projetobeta.barbersystem.general.AbreTela;

public class Clientes extends Fragment {
    private FrameLayout listarClientes;
    private FrameLayout cadastrarCliente;
    private FrameLayout agenda;
    private Toolbar toolbar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       return inflater.inflate(R.layout.clientes,container,false);
    }

    @Override
    public void onStart() {
        iniciarObjetos();
        setCadastrarCliente();
        setToolbar();
        setAgenda();
        setListarClientes();
        super.onStart();
    }

    private void iniciarObjetos(){
        listarClientes = (FrameLayout) getActivity().findViewById(R.id.listarClientes);
        cadastrarCliente = (FrameLayout) getActivity().findViewById(R.id.cadastrarCliente);
        agenda = (FrameLayout) getActivity().findViewById(R.id.atendimentos);
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbarCliente);
    }

    private void setToolbar(){
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                getActivity().finish();
                return true;
            default: return super.onOptionsItemSelected(item);
        }
    }

    private void setCadastrarCliente(){
        cadastrarCliente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AbreTela(getActivity().getSupportFragmentManager(),new Cadastrar_Cliente(),R.id.setTelaClientes);
            }
        });
    }

    private void setAgenda(){
        agenda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    private void setListarClientes(){
        listarClientes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AbreTela(getActivity().getSupportFragmentManager(),new Lista_Clientes(),R.id.setTelaClientes);
            }
        });
    }
}
