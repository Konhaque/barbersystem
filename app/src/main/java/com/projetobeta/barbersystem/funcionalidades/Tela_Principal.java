package com.projetobeta.barbersystem.funcionalidades;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.projetobeta.barbersystem.Balanco;
import com.projetobeta.barbersystem.Clientes;
import com.projetobeta.barbersystem.Pagamentos;
import com.projetobeta.barbersystem.R;
import com.projetobeta.barbersystem.Registros;
import com.projetobeta.barbersystem.funcionalidades.registro.Registro;
import com.projetobeta.barbersystem.general.AbreTela;

public class Tela_Principal extends Fragment {
    private ImageView sair;
    private ImageView clientes;
    private ImageView registro;
    private ImageView pagamentos;
    private ImageView balanco;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return (ViewGroup) inflater.inflate(R.layout.tela_principal,container,false);
    }

    @Override
    public void onStart() {
        iniciarObjetos();
        setClientes();
        setRegistro();
        setPagamentos();
        setBalanco();
        super.onStart();
    }


    private void iniciarObjetos(){
        clientes = (ImageView) getActivity().findViewById(R.id.clientes);
        registro = (ImageView) getActivity().findViewById(R.id.registro);
        pagamentos = (ImageView) getActivity().findViewById(R.id.pagamento);
        balanco = (ImageView) getActivity().findViewById(R.id.balanca);
        sair = (ImageView) getActivity().findViewById(R.id.sair);
    }

    private void setClientes(){
        clientes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), Clientes.class));
            }
        });
    }

    private void setRegistro(){
        registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), Registros.class));
            }
        });
    }

    private void setPagamentos(){
        pagamentos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), Pagamentos.class));
            }
        });
    }

    private void setBalanco(){
        balanco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), Balanco.class));
            }
        });
    }


}
