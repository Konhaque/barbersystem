package com.projetobeta.barbersystem.funcionalidades.pagamentos;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.projetobeta.barbersystem.R;
import com.projetobeta.barbersystem.bd.Lancamento;
import com.projetobeta.barbersystem.bd.Repository;
import com.projetobeta.barbersystem.bd.Servicos;
import com.projetobeta.barbersystem.general.AbreTela;

import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Adicionar_Lancamento extends Fragment {

    private Button adicionarCliente;
    private Toolbar toolbar;
    private TextView nomeCliente;
    private TextView enderecoCliente;
    private Spinner servicos;
    private Button adicionarServico;
    private LinearLayout listaServicos;
    private TextView total;
    private Button adicionarLancamento;
    private AlertDialog dialog;
    private ArrayList<Servicos> list;
    private TextView dadosCliente;
    private double valorTotal;
    private List<String> servicosPrestados;
    private Lancamento lancamento;




    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.adicionar_lancamento,container,false);
    }

    @Override
    public void onStart() {
        iniciarObjetos();
        setToolbar();
        getServicos();
        setTotal();
        setAdicionarServico();
        setAdicionarCliente();
        setAdicionarLancamento();
        super.onStart();
    }

    private void iniciarObjetos(){
        adicionarCliente = (Button) getActivity().findViewById(R.id.adicionarCliente);
        nomeCliente = (TextView) getActivity().findViewById(R.id.nomeCliente);
        enderecoCliente = (TextView) getActivity().findViewById(R.id.enderecoCliente);
        servicos = (Spinner) getActivity().findViewById(R.id.servico);
        adicionarServico = (Button) getActivity().findViewById(R.id.adicionarServico);
        listaServicos = (LinearLayout) getActivity().findViewById(R.id.listaServicos);
        total = (TextView) getActivity().findViewById(R.id.total);
        adicionarLancamento = (Button) getActivity().findViewById(R.id.adicionarLancamento);
        dadosCliente = (TextView) getActivity().findViewById(R.id.textView);
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbarLancamento);
        list = new ArrayList<>();
        servicosPrestados = new ArrayList<>();
        lancamento = new Lancamento();
    }

    private void setToolbar(){
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                new AbreTela(getActivity().getSupportFragmentManager(),new Pagamentos(),R.id.setTelaPagamentos);
                return true;
            default: return super.onOptionsItemSelected(item);
        }
    }

    private void setAdicionarCliente(){
        adicionarCliente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pesquisaCliente();
            }
        });
    }

    private void setTotal(){
        total.setText("Total: "+valorTotal);
    }


    private void pesquisaCliente(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.pesquisar_cliente, null);
        final EditText editText = view.findViewById(R.id.pesquisaNome);
        ImageView imageView = view.findViewById(R.id.pesquiser);
        final LinearLayout linearLayout = view.findViewById(R.id.listaCliesntes);
        carregarClientes(linearLayout);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pesquisar(editText.getText().toString(), linearLayout);
            }
        });
        builder.setView(view);
        builder.setCancelable(true);
        dialog = builder.create();
        dialog.show();
    }

    private void carregarClientes(final LinearLayout linearLayout){
        linearLayout.removeAllViews();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Clientes");
        reference.orderByChild("nome").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (final DataSnapshot dataSnapshot : snapshot.getChildren()){
                    if(dataSnapshot.child("idUsuario").getValue().toString().equals(new Repository(getContext()).getUsuario())) {
                        TextView textView = new TextView(getContext());
                        textView.setText(dataSnapshot.child("nome").getValue().toString());
                        Typeface typeface = ResourcesCompat.getFont(getContext(), R.font.kirsty_bd);
                        textView.setTypeface(typeface);
                        textView.setTextColor(Color.WHITE);
                        textView.setGravity(Gravity.CENTER);
                        textView.setTextSize(20);
                        textView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                nomeCliente.setText("Nome: " + dataSnapshot.child("nome").getValue().toString());
                                enderecoCliente.setText("Endereço: " + dataSnapshot.child("logradouro").getValue().toString());
                                dadosCliente.setVisibility(View.VISIBLE);
                                nomeCliente.setVisibility(View.VISIBLE);
                                enderecoCliente.setVisibility(View.VISIBLE);
                                adicionarCliente.setText("Alterar Cliente");
                                lancamento.setIdCliente(dataSnapshot.getKey());
                                dialog.dismiss();
                            }
                        });
                        linearLayout.addView(textView);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    private void pesquisar(final String string, final LinearLayout linearLayout){
        linearLayout.removeAllViews();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Clientes");
        reference.orderByChild("nome").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(final DataSnapshot dataSnapshot : snapshot.getChildren()){
                    if(dataSnapshot.child("nome").getValue().toString().contains(string) &&
                            dataSnapshot.child("idUsuario").getValue().toString().equals(new Repository(getContext()).getUsuario())){
                        TextView textView = new TextView(getContext());
                        textView.setText(dataSnapshot.child("nome").getValue().toString());
                        Typeface typeface = ResourcesCompat.getFont(getContext(),R.font.kirsty_bd);
                        textView.setTypeface(typeface);
                        textView.setTextColor(Color.WHITE);
                        textView.setGravity(Gravity.CENTER);
                        textView.setTextSize(20);
                        textView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                nomeCliente.setText("Nome: "+dataSnapshot.child("nome").getValue().toString());
                                enderecoCliente.setText("Endereço: "+dataSnapshot.child("logradouro").getValue().toString());
                                dadosCliente.setVisibility(View.VISIBLE);
                                nomeCliente.setVisibility(View.VISIBLE);
                                enderecoCliente.setVisibility(View.VISIBLE);
                                dialog.dismiss();
                            }
                        });
                        linearLayout.addView(textView);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }


    private void getServicos(){
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Servicos");
        reference.orderByChild("servico").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot dataSnapshot : snapshot.getChildren()){
                    if(dataSnapshot.child("idUsuario").getValue().toString().equals(new Repository(getContext()).getUsuario())) {
                        Servicos servicos = new Servicos();
                        servicos.setServico(dataSnapshot.child("servico").getValue().toString());
                        servicos.setValor(Double.parseDouble(dataSnapshot.child("valor").getValue().toString()));
                        verificaPromocao(dataSnapshot.getKey(), servicos);
                        list.add(servicos);
                    }
                }
                setServicos();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void setServicos(){
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(), R.layout.spinner_item, carregarServicos());
        arrayAdapter.setDropDownViewResource(R.layout.spinner_dropdown);
        servicos.setAdapter(arrayAdapter);
    }

    private void verificaPromocao(final String id, final Servicos servicos){
        final SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        final Date data = new Date();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Promocoes");
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                SimpleDateFormat dataFinal = new SimpleDateFormat("dd/MM/yyyy");
                for(DataSnapshot dataSnapshot : snapshot.getChildren()){
                    try {
                        if(snapshot.child("idUsuario").getValue().toString().equals(new Repository(getContext()).getUsuario())) {
                            if (id.equals(dataSnapshot.child("idServico").getValue().toString())) {
                                Date dtFinal = dataFinal.parse(dataSnapshot.child("fimPromocao").getValue().toString());
                                Date date = dataFinal.parse(format.format(data));
                                if (!date.after(dtFinal))
                                    servicos.setValor(Double.parseDouble(dataSnapshot.child("valor").getValue().toString()));
                            }
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private ArrayList<String> carregarServicos(){
        ArrayList<String> servicos = new ArrayList<>();
        for (int i = 0; i< list.size();i++){
            servicos.add(list.get(i).getServico());
        }
        return servicos;
    }

    private void setAdicionarServico(){
        adicionarServico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(valorTotal == 0) listaServicos.setVisibility(View.VISIBLE);
                adicionarServico(servicos.getSelectedItemPosition());
            }
        });
    }

    private void adicionarServico(int id){
        final TextView textView = new TextView(getContext());
        textView.setText(list.get(id).getServico() + " - "+ list.get(id).getValor());
        textView.setTextColor(Color.WHITE);
        Typeface typeface = ResourcesCompat.getFont(getContext(),R.font.kirsty_bd);
        textView.setTypeface(typeface);
        textView.setTextSize(18);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removerServico(textView);
            }
        });
        listaServicos.addView(textView);
        valorTotal += list.get(id).getValor();
        setTotal();
    }

    private void removerServico(TextView textView){
        for (int i=0; i<list.size();i++){
            String string = list.get(i).getServico() + " - "+ list.get(i).getValor();
            if(string.equalsIgnoreCase(textView.getText().toString())){
                valorTotal -= list.get(i).getValor();
                listaServicos.removeView(textView);
                setTotal();
            }
        }
    }

    private void setAdicionarLancamento(){
        adicionarLancamento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                LayoutInflater inflater = getLayoutInflater();
                builder.setView(inflater.inflate(R.layout.activity_main,null));
                builder.setCancelable(true);
                dialog = builder.create();
                dialog.show();
                int i = 1;
                TextView textView = (TextView) listaServicos.getChildAt(i);
                while (textView != null){
                    servicosPrestados.add(textView.getText().toString());
                    i++;
                    textView = (TextView) listaServicos.getChildAt(i);
                }
                verifica();
            }
        });
    }

    private void verifica(){
        if(nomeCliente.getVisibility() == View.GONE) alertErro("Por favor adicione um cliente");
        else if(valorTotal == 0) alertErro("O valor do serviço não pode ser R$0,00");
        else salvar();
    }

    private void alertErro(String erro){
        dialog.dismiss();
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Algo deu errado");
        builder.setMessage(erro);
        builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        dialog = builder.create();
        dialog.show();
    }

    private void salvar(){
        lancamento();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Lançamentos");
        reference.child(reference.push().getKey()).setValue(lancamento).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                alertaSucesso();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
              Log.i("Error",e.getMessage());
            }
        });
    }

    private void lancamento(){
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        lancamento.setIdUsuario(new Repository(getContext()).getUsuario());
        lancamento.setDataLancamento(dateFormat.format(date));
        lancamento.setServicos(servicosPrestados);
        lancamento.setValorTotal(valorTotal);
    }

    private void alertaSucesso(){
        dialog.dismiss();
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater layoutInflater = getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.alerta_sucesso, null);
        TextView textView = view.findViewById(R.id.textAlerta);
        ImageView imageView = view.findViewById(R.id.imgAlerta);
        Button sair = view.findViewById(R.id.sair);
        sair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                new AbreTela(getActivity().getSupportFragmentManager(),new Adicionar_Lancamento(),R.id.setTelaPagamentos);
            }
        });
        imageView.setImageDrawable(getActivity().getDrawable(R.drawable.dinheiro_branco));
        textView.setText("Lançamento efetuado com sucesso!");
        builder.setView(view);
        builder.setCancelable(true);
        dialog = builder.create();
        dialog.show();
    }






}
