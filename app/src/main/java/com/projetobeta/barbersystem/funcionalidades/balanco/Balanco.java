package com.projetobeta.barbersystem.funcionalidades.balanco;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.projetobeta.barbersystem.R;
import com.projetobeta.barbersystem.general.AbreTela;

public class Balanco extends Fragment {
    private Toolbar toolbar;
    private FrameLayout relatorioDiario;
    private FrameLayout relatorioMensal;
    private FrameLayout relatorioAnual;
    private FrameLayout totalDespesas;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.balanco,container,false);
    }

    @Override
    public void onStart() {
        iniciarObjetetos();
        setToolbar();
        setRelatorioDiario();
        super.onStart();
    }

    private void iniciarObjetetos(){
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbarBalanco);
        relatorioDiario = (FrameLayout) getActivity().findViewById(R.id.relatorioDiario);
    }

    private void setToolbar(){
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                getActivity().finish();
                return true;
            default: return super.onOptionsItemSelected(item);
        }
    }

    private void setRelatorioDiario(){
        relatorioDiario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AbreTela(getActivity().getSupportFragmentManager(),new Atendimentos_Dia(),R.id.setTelaBalanco);
            }
        });
    }

}
