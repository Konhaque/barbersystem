
package com.projetobeta.barbersystem.funcionalidades.clientes;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.projetobeta.barbersystem.R;
import com.projetobeta.barbersystem.bd.Repository;
import com.projetobeta.barbersystem.general.AbreTela;

public class Lista_Clientes extends Fragment {

    private Toolbar toolbar;
    private LinearLayout clientes;
    private Dialog dialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.lista_clientes,container,false);
    }

    @Override
    public void onStart() {
        iniciarObjetos();
        setToolbar();
        getClientes();
        super.onStart();
    }

    private void iniciarObjetos(){
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbarListaClientes);
        clientes = (LinearLayout) getActivity().findViewById(R.id.llClientes);
    }

    private void setToolbar(){
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                new AbreTela(getActivity().getSupportFragmentManager(),new Clientes(),R.id.setTelaClientes);
                return true;
            default: return super.onOptionsItemSelected(item);
        }
    }

    private void getClientes(){
        clientes.removeAllViews();
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = getLayoutInflater();
        builder.setView(inflater.inflate(R.layout.activity_main,null));
        builder.setCancelable(true);
        dialog = builder.create();
        dialog.show();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Clientes");
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot ds : snapshot.getChildren()){
                    if(ds.child("idUsuario").getValue().toString().equals(new Repository(getContext()).getUsuario())) adicionar(ds);
                }
                dialog.dismiss();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void adicionar (final DataSnapshot snapshot){
        FrameLayout frameLayout = new FrameLayout(getContext());
        TextView textView = new TextView(getContext());
        Typeface typeface = ResourcesCompat.getFont(getContext(),R.font.kirsty_bd);
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) clientes.getLayoutParams();
        layoutParams.setMargins(0,20,0,0);
        frameLayout.setBackground(getActivity().getDrawable(R.drawable.container));
        frameLayout.setPadding(0,20,0,20);
        frameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                detalhesClientes(snapshot);
            }
        });
        textView.setText(snapshot.child("nome").getValue().toString());
        textView.setGravity(Gravity.CENTER);
        textView.setTypeface(typeface);
        textView.setTextColor(Color.BLACK);
        frameLayout.addView(textView);
        clientes.setPadding(20,20,20,20);
        clientes.addView(frameLayout,layoutParams);
    }

    private void detalhesClientes(DataSnapshot snapshot){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.cadastrar_cliente,null);
        EditText nome = view.findViewById(R.id.nome);
        EditText cpf = view.findViewById(R.id.cpf);
        EditText data_nascimento = view.findViewById(R.id.data_nascimento);
        EditText cep = view.findViewById(R.id.cep);
        EditText logradouro = view.findViewById(R.id.logradouro);
        EditText bairro = view.findViewById(R.id.bairro);
        EditText cidade = view.findViewById(R.id.cidade);
        Button sair = view.findViewById(R.id.salvarCliente);
        nome.setText("Nome: "+snapshot.child("nome").getValue().toString());
        nome.setEnabled(false);
        cpf.setText("CPF: "+snapshot.child("cpf").getValue().toString());
        cpf.setEnabled(false);
        data_nascimento.setText("Data de Nascimento: "+snapshot.child("dataNascimento").getValue().toString());
        data_nascimento.setEnabled(false);
        cep.setText("CEP: "+snapshot.child("cep").getValue().toString());
        cep.setEnabled(false);
        logradouro.setText("Logradouro: "+snapshot.child("logradouro").getValue().toString());
        logradouro.setEnabled(false);
        bairro.setText("Bairro: "+snapshot.child("bairro").getValue().toString());
        bairro.setEnabled(false);
        cidade.setText("Cidade: "+snapshot.child("cidade").getValue().toString());
        cidade.setEnabled(false);
        sair.setText("Sair");
        sair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        builder.setView(view);
        builder.setCancelable(true);
        dialog  = builder.create();
        dialog.show();
    }



}
