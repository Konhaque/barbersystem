package com.projetobeta.barbersystem.funcionalidades.pagamentos;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.projetobeta.barbersystem.R;
import com.projetobeta.barbersystem.bd.Repository;
import com.projetobeta.barbersystem.general.AbreTela;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Promocao extends Fragment {

    private Toolbar toolbar;
    private Spinner servicos;
    private EditText inicioPromocao;
    private EditText fimPromocao;
    private EditText valor;
    private Button cadastrar;
    private ArrayList<String> idServico;
    private AlertDialog dialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.promocao,container,false);
    }

    @Override
    public void onStart() {
        iniciarObjetos();
        setToolbar();
        getServicos();
        setInicioPromocao();
        setFimPromocao();
        setCadastrar();
        super.onStart();
    }

    private void iniciarObjetos(){
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbarPromocao);
        servicos = (Spinner) getActivity().findViewById(R.id.servico);
        inicioPromocao = (EditText) getActivity().findViewById(R.id.inicioPromocao);
        fimPromocao = (EditText) getActivity().findViewById(R.id.fimPromocao);
        valor = (EditText) getActivity().findViewById(R.id.valorPromocao);
        cadastrar = (Button) getActivity().findViewById(R.id.cadastrarPromocao);
        idServico = new ArrayList<String>();
    }

    private void setToolbar(){
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                new AbreTela(getActivity().getSupportFragmentManager(),new Pagamentos(),R.id.setTelaPagamentos);
                return true;
            default: return super.onOptionsItemSelected(item);
        }
    }

    private void getServicos(){
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Servicos");
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            ArrayList<String> lista = new ArrayList<>();
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot dataSnapshot : snapshot.getChildren()){
                    lista.add(dataSnapshot.child("servico").getValue().toString());
                    idServico.add(dataSnapshot.getKey());
                }
                setServicos(lista);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    private void setServicos(ArrayList<String> lista){
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(),R.layout.spinner_item,lista);
        arrayAdapter.setDropDownViewResource(R.layout.spinner_dropdown);
        servicos.setAdapter(arrayAdapter);
    }

    private void setInicioPromocao(){
        Calendar calendar = Calendar.getInstance();
        int ano = calendar.get(Calendar.YEAR);
        int mes = calendar.get(Calendar.MONTH);
        int dia = calendar.get(Calendar.DAY_OF_MONTH);
        final Dialog dialog = new DatePickerDialog(getContext(), android.R.style.Theme_Holo_Light_Dialog_MinWidth, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                if((i1+1) <= 9) inicioPromocao.setText(i2+"/0"+(i1+1)+"/"+i);
                else inicioPromocao.setText(i2+"/"+(i1+1)+"/"+i);
            }
        },ano,mes,dia);
        inicioPromocao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });
    }

    private void setFimPromocao(){
        Calendar calendar = Calendar.getInstance();
        int ano = calendar.get(Calendar.YEAR);
        int mes = calendar.get(Calendar.MONTH);
        int dia = calendar.get(Calendar.DAY_OF_MONTH);
        final Dialog dialog = new DatePickerDialog(getContext(), android.R.style.Theme_Holo_Light_Dialog_MinWidth, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                if((i1+1) <= 9) fimPromocao.setText(i2+"/0"+(i1+1)+"/"+i);
                else fimPromocao.setText(i2+"/"+(i1+1)+"/"+i);
            }
        },ano,mes,dia);
        fimPromocao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

    }

    private void setCadastrar(){
        cadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                LayoutInflater inflater = getLayoutInflater();
                builder.setView(inflater.inflate(R.layout.activity_main,null));
                builder.setCancelable(true);
                dialog = builder.create();
                dialog.show();
                verificar();
            }
        });

    }

    private void verificar(){
        if(servicos.getSelectedItem().toString().equals("")) dialog.dismiss();
        else if (inicioPromocao.getText().length() == 0 || inicioPromocao.getText().toString().equals(" ")) setError(inicioPromocao);
        else if (fimPromocao.getText().length() == 0 || fimPromocao.getText().toString().equals(" ")) setError(fimPromocao);
        else if (valor.getText().length() == 0 || valor.getText().toString().equals(" ")) setError(valor);
        else salvar();

    }

    private void setError(EditText editText){
        editText.setError(getString(R.string.campo_sem_preencher));
        editText.requestFocus();
        dialog.dismiss();
    }

    private void salvar(){
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Promocoes");
        reference.child(reference.push().getKey()).setValue(carregarPromocao()).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                dialog.dismiss();
                alertaSucesso();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });


    }

    private com.projetobeta.barbersystem.bd.Promocao carregarPromocao(){
        com.projetobeta.barbersystem.bd.Promocao promocao = new com.projetobeta.barbersystem.bd.Promocao();
        promocao.setIdServico(new Repository(getContext()).getUsuario());
        promocao.setIdServico(idServico.get(servicos.getSelectedItemPosition()));
        promocao.setInicioPromocao(inicioPromocao.getText().toString());
        promocao.setFimPromocao(fimPromocao.getText().toString());
        promocao.setValor(Double.parseDouble(valor.getText().toString()));
        return promocao;
    }

    private void alertaSucesso(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.alerta_sucesso, null);
        TextView textView = view.findViewById(R.id.textAlerta);
        ImageView imageView = view.findViewById(R.id.imgAlerta);
        Button button = view.findViewById(R.id.sair);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                inicioPromocao.getText().clear();
                fimPromocao.getText().clear();
                valor.getText().clear();
            }
        });
        textView.setText("Promoção cadastrada com sucesso!");
        imageView.setImageDrawable(getActivity().getDrawable(R.drawable.dinheiro_branco));
        builder.setView(view);
        builder.setCancelable(true);
        dialog = builder.create();
        dialog.show();
    }





}
