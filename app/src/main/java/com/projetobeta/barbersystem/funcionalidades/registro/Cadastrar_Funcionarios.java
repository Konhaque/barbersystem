package com.projetobeta.barbersystem.funcionalidades.registro;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.projetobeta.barbersystem.R;
import com.projetobeta.barbersystem.bd.Funcionarios;
import com.projetobeta.barbersystem.bd.Repository;
import com.projetobeta.barbersystem.general.AbreTela;

import java.util.Calendar;

public class Cadastrar_Funcionarios extends Fragment {

    private EditText nome;
    private EditText dataNascimento;
    private EditText cpf;
    private EditText cargo;
    private CheckBox ativo;
    private Button salvar;
    private Toolbar toolbar;
    private AlertDialog dialog;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.cadastrar_funcionario,container,false);
    }

    @Override
    public void onStart() {
        iniciarObjetos();
        setToolbar();
        setDataNascimento();
        setSalvar();
        super.onStart();
    }

    private void iniciarObjetos(){
        nome = (EditText) getActivity().findViewById(R.id.nome);
        dataNascimento = (EditText) getActivity().findViewById(R.id.data_nascimento);
        cpf = (EditText) getActivity().findViewById(R.id.CPF);
        cargo = (EditText) getActivity().findViewById(R.id.cargo);
        ativo = (CheckBox) getActivity().findViewById(R.id.ativo);
        salvar = (Button) getActivity().findViewById(R.id.salvarFuncionario);
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar_cf);
    }

    private void setToolbar(){
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                new AbreTela(getActivity().getSupportFragmentManager(),new Registro(),R.id.setTelaRegistros);
                return true;
            default: return super.onOptionsItemSelected(item);
        }
    }

    private void setDataNascimento(){
        Calendar calendar = Calendar.getInstance();
        int ano = calendar.get(Calendar.YEAR);
        int mes = calendar.get(Calendar.MONTH);
        int dia = calendar.get(Calendar.DAY_OF_MONTH);
        final Dialog dialog = new DatePickerDialog(getContext(), android.R.style.Theme_Holo_Light_Dialog_MinWidth, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                dataNascimento.setText(i2 + "/"+(i1+1)+"/"+i);
            }
        },ano,mes,dia);
        dataNascimento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });
    }

    private void setSalvar(){
        salvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                LayoutInflater inflater = getLayoutInflater();
                builder.setView(inflater.inflate(R.layout.activity_main,null));
                builder.setCancelable(true);
                dialog = builder.create();
                dialog.show();
                validar();
            }
        });
    }

    private void validar(){
        if(nome.getText().length() == 0 || nome.getText().toString().equals(" ")) setError(nome);
        else if(dataNascimento.getText().length() == 0 || dataNascimento.getText().equals(" ")) setError(dataNascimento);
        else if((cpf.getText().length() == 0 || cpf.getText().length() < 11) || cpf.getText().equals(" ")) setError(cpf);
        else if(cargo.getText().length() == 0 || cargo.getText().equals(" ")) setError(cargo);
        else salvar();
    }

    private void setError(EditText editText){
        editText.setError(getString(R.string.campo_sem_preencher));
        editText.requestFocus();
        dialog.dismiss();
    }

    private void salvar(){
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Funcionarios");
        databaseReference.child(databaseReference.push().getKey()).setValue(carregarFuncionario()).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                dialog.dismiss();
                alertaSucesso();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });
    }

    private Funcionarios carregarFuncionario(){
        Funcionarios funcionario = new Funcionarios();
        funcionario.setIdUsuario(new Repository(getContext()).getUsuario());
        funcionario.setNome(nome.getText().toString());
        funcionario.setDataNascimento(dataNascimento.getText().toString());
        funcionario.setCpf(cpf.getText().toString());
        funcionario.setCargo(cargo.getText().toString());
        funcionario.setAtivo(ativo.isChecked());
        return funcionario;
    }

    private void gerarUsuario(){
        
    }


    private void alertaSucesso(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.alerta_sucesso,null);
        TextView textView = view.findViewById(R.id.textAlerta);
        ImageView imageView = view.findViewById(R.id.imgAlerta);
        Button sair = view.findViewById(R.id.sair);
        sair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nome.getText().clear();
                dataNascimento.getText().clear();
                cpf.getText().clear();
                cargo.getText().clear();
                ativo.setChecked(true);
                dialog.dismiss();
            }
        });
        textView.setText("Funcionário salvo com sucesso!");
        imageView.setImageDrawable(getActivity().getDrawable(R.drawable.funcionarios));
        builder.setView(view);
        builder.setCancelable(true);
        dialog = builder.create();
        dialog.show();
    }



}
