package com.projetobeta.barbersystem.funcionalidades.registro;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.projetobeta.barbersystem.R;
import com.projetobeta.barbersystem.funcionalidades.Tela_Principal;
import com.projetobeta.barbersystem.funcionalidades.registro.Cadastrar_Funcionarios;
import com.projetobeta.barbersystem.funcionalidades.registro.Cadastrar_Produto;
import com.projetobeta.barbersystem.funcionalidades.registro.Cadastrar_Servico;
import com.projetobeta.barbersystem.general.AbreTela;

public class Registro extends Fragment {
    private Toolbar toolbar;
    private FrameLayout cadastrarServicos;
    private FrameLayout cadastrarProdutos;
    private FrameLayout cadastrarFuncionarios;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.registro,container,false);
    }

    @Override
    public void onStart() {
        iniciar_Objetos();
        setToolbar();
        setCadastrarServicos();
        setCadastrarProdutos();
        setCadastrarFuncionarios();
        super.onStart();
    }

    private void iniciar_Objetos(){
        cadastrarFuncionarios = (FrameLayout) getActivity().findViewById(R.id.cadastrarFuncionarios);
        cadastrarProdutos = (FrameLayout) getActivity().findViewById(R.id.cadastrarProdutos);
        cadastrarServicos = (FrameLayout) getActivity().findViewById(R.id.cadastrarServicos);
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
    }

    private void setCadastrarServicos(){
        cadastrarServicos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AbreTela(getActivity().getSupportFragmentManager(),new Cadastrar_Servico(),R.id.setTelaRegistros);
            }
        });
    }

    private void setCadastrarProdutos(){
        cadastrarProdutos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AbreTela(getActivity().getSupportFragmentManager(),new Cadastrar_Produto(),R.id.setTelaRegistros);
            }
        });
    }

    private void setCadastrarFuncionarios(){
        cadastrarFuncionarios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AbreTela(getActivity().getSupportFragmentManager(),new Cadastrar_Funcionarios(),R.id.setTelaRegistros);
            }
        });
    }


    private void setToolbar(){
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                getActivity().finish();
                return true;
            default: return super.onOptionsItemSelected(item);
        }
    }
}
