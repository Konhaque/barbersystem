package com.projetobeta.barbersystem.funcionalidades.balanco;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.projetobeta.barbersystem.R;
import com.projetobeta.barbersystem.bd.Repository;
import com.projetobeta.barbersystem.general.AbreTela;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.EventListener;
import java.util.List;
import java.util.Map;

public class Atendimentos_Dia extends Fragment {

    private Toolbar toolbar;
    private TextView textRelatorio;
    private LinearLayout linearLayout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.atendimentos_dia,container,false);
    }

    @Override
    public void onStart() {
        iniciarObjetos();
        setToolbar();
        setTextRelatorio();
        getServicosDia();
        super.onStart();
    }

    private void iniciarObjetos(){
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbarAtendimentosDia);
        textRelatorio = (TextView) getActivity().findViewById(R.id.textRelatorio);
        linearLayout = (LinearLayout) getActivity().findViewById(R.id.llRelatorioDiario);
    }

    private void setToolbar(){
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                new AbreTela(getActivity().getSupportFragmentManager(),new Balanco(),R.id.setTelaBalanco);
                return true;
            default: return super.onOptionsItemSelected(item);
        }
    }

    private void setTextRelatorio(){
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        textRelatorio.setText("Relatório dia: "+format.format(date));
    }

    private void getServicosDia(){
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Lançamentos");
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Date date = new Date();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                for(DataSnapshot dataSnapshot : snapshot.getChildren()){
                    if(dataSnapshot.child("idUsuario").getValue().toString().equals(new Repository(getContext()).getUsuario())){
                        if(dataSnapshot.child("dataLancamento").getValue().toString().equals(simpleDateFormat.format(date))) adicionar(dataSnapshot);
                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    private void adicionar(final DataSnapshot snapshot){
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Clientes/"+snapshot.child("idCliente").getValue().toString());
        reference.child("nome").get().addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DataSnapshot> task) {
                TextView cliente = new TextView(getContext());
                Typeface typeface = ResourcesCompat.getFont(getContext(),R.font.kirsty_bd);
                cliente.setTypeface(typeface);
                cliente.setText(task.getResult().getValue().toString());
                cliente.setTextColor(Color.WHITE);
                cliente.setTextSize(18);
                cliente.setPadding(20,0,0,0);
                linearLayout.addView(cliente);
                List<String> servicos = new ArrayList<>();
                GenericTypeIndicator<List<String>> genericTypeIndicator = new GenericTypeIndicator<List<String>>() {
                };
                servicos = snapshot.child("servicos").getValue(genericTypeIndicator);
                for(int i = 0; i< servicos.size();i++){
                    TextView servico = new TextView(getContext());
                    servico.setTypeface(typeface);
                    servico.setText(servicos.get(i));
                    servico.setTextColor(Color.WHITE);
                    servico.setPadding(40,10,0,0);
                    linearLayout.addView(servico);
                }
                TextView toltalServico = new TextView(getContext());
                toltalServico.setGravity(Gravity.CENTER);
                toltalServico.setText("Total: "+ snapshot.child("valorTotal").getValue().toString());
                toltalServico.setTypeface(typeface);
                toltalServico.setPadding(0,20,0,20);
                toltalServico.setTextColor(Color.WHITE);
                linearLayout.addView(toltalServico);
            }
        });

    }
}
