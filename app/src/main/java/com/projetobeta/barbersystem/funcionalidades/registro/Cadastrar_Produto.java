package com.projetobeta.barbersystem.funcionalidades.registro;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.projetobeta.barbersystem.R;
import com.projetobeta.barbersystem.bd.Produtos;
import com.projetobeta.barbersystem.bd.Repository;
import com.projetobeta.barbersystem.general.AbreTela;

public class Cadastrar_Produto extends Fragment {

    private Toolbar toolbar;
    private EditText produto;
    private EditText valor;
    private EditText quantidade;
    private Button salvar;
    private AlertDialog dialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.cadastrar_produto,container,false);
    }

    @Override
    public void onStart() {
        iniciar_Obejtos();
        setToolbar();
        setSalvar();
        super.onStart();
    }

    private void iniciar_Obejtos(){
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar_cp);
        produto = (EditText) getActivity().findViewById(R.id.produto);
        valor = (EditText) getActivity().findViewById(R.id.preco);
        quantidade = (EditText) getActivity().findViewById(R.id.quantidade);
        salvar = (Button) getActivity().findViewById(R.id.salvar);
    }

    private void setToolbar(){
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                new AbreTela(getActivity().getSupportFragmentManager(),new Registro(),R.id.setTelaRegistros);
                return true;
            default: return super.onOptionsItemSelected(item);
        }
    }

    private void setSalvar(){
        salvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                LayoutInflater inflater = getLayoutInflater();
                builder.setView(inflater.inflate(R.layout.activity_main,null));
                builder.setCancelable(true);
                dialog = builder.create();
                dialog.show();
                verificar();
            }
        });
    }

    private void verificar(){
        if(produto.getText().length() == 0 || produto.getText().toString().equals(" ")) setError(produto);
        else if(valor.getText().length() == 0 || valor.getText().toString().equals(" ")) setError(valor);
        else if(quantidade.getText().length() == 0 || quantidade.getText().toString().equals(" ")) setError(quantidade);
        else salvar();
    }

    private void setError(EditText editText){
        editText.setError("Este campo precisa ser preenchido");
        editText.requestFocus();
        dialog.dismiss();
    }

    private void salvar(){
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Produtos");
        databaseReference.child(databaseReference.push().getKey()).setValue(carregarProduto()).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                dialog.dismiss();
                alertaSucesso();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });

    }

    private Produtos carregarProduto(){
        Produtos produtos = new Produtos();
        produtos.setIdUsuario(new Repository(getContext()).getUsuario());
        produtos.setProduto(produto.getText().toString());
        produtos.setPreco(Double.parseDouble(valor.getText().toString()));
        produtos.setQuantidade(Integer.parseInt(quantidade.getText().toString()));
        return produtos;
    }

    private void alertaSucesso(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.alerta_sucesso,null);
        TextView textView = view.findViewById(R.id.textAlerta);
        ImageView imageView = view.findViewById(R.id.imgAlerta);
        Button sair = view.findViewById(R.id.sair);
        sair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                produto.getText().clear();
                valor.getText().clear();
                quantidade.getText().clear();
                dialog.dismiss();
            }
        });
        textView.setText("Produto salvo com sucesso!");
        imageView.setImageDrawable(getActivity().getDrawable(R.drawable.produtos));
        builder.setView(view);
        builder.setCancelable(true);
        dialog = builder.create();
        dialog.show();
    }

}
