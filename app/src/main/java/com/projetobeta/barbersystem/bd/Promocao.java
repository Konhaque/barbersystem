package com.projetobeta.barbersystem.bd;

public class Promocao {
    private String idUsuario;
    private String idServico;
    private String inicioPromocao;
    private String fimPromocao;
    private double valor;

    public String getIdServico() {
        return idServico;
    }

    public void setIdServico(String idServico) {
        this.idServico = idServico;
    }

    public String getInicioPromocao() {
        return inicioPromocao;
    }

    public void setInicioPromocao(String inicioPromocao) {
        this.inicioPromocao = inicioPromocao;
    }

    public String getFimPromocao() {
        return fimPromocao;
    }

    public void setFimPromocao(String fimPromocao) {
        this.fimPromocao = fimPromocao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
