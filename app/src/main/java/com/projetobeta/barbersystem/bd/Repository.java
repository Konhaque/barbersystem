package com.projetobeta.barbersystem.bd;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import androidx.annotation.Nullable;

public class Repository {
    private DaoUtil daoUtil;

    public Repository(@Nullable Context context){
        daoUtil = new DaoUtil(context);
    }

    public void criarUsuario(){
        String sql  = "DROP TABLE IF EXISTS TB_USUARIO";
        daoUtil.getConexaoDataBase().execSQL(sql);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("CREATE TABLE TB_USUARIO(");
        stringBuilder.append("ID_USUARIO TEXT NOT NULL)");
        daoUtil.getConexaoDataBase().execSQL(stringBuilder.toString());
    }

    public void salvarUsuario(String id_usuario){
        daoUtil.getConexaoDataBase().insert("TB_USUARIO",null,carregaUsuario(id_usuario));
    }

    private ContentValues carregaUsuario(String id_usuario){
        ContentValues cv = new ContentValues();
        cv.put("ID_USUARIO",id_usuario);
        return cv;
    }

    public String getUsuario(){
        String sql = "SELECT * FROM TB_USUARIO";
        Cursor cursor = daoUtil.getConexaoDataBase().rawQuery(sql,null);
        cursor.moveToFirst();
        return cursor.getString(cursor.getColumnIndex("ID_USUARIO"));
    }
}
