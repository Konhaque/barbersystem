package com.projetobeta.barbersystem;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.projetobeta.barbersystem.general.AbreTela;
import com.projetobeta.barbersystem.general.FullScrean;

public class Pagamentos extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new FullScrean(this);
        setContentView(R.layout.activity_pagamentos);
        new AbreTela(getSupportFragmentManager(), new com.projetobeta.barbersystem.funcionalidades.pagamentos.Pagamentos(),R.id.setTelaPagamentos);
    }


}