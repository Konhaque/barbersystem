package com.projetobeta.barbersystem;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.projetobeta.barbersystem.funcionalidades.registro.Registro;
import com.projetobeta.barbersystem.general.AbreTela;
import com.projetobeta.barbersystem.general.FullScrean;

public class Registros extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new FullScrean(this);
        setContentView(R.layout.activity_registros);
        new AbreTela(getSupportFragmentManager(),new Registro(),R.id.setTelaRegistros);
    }
}