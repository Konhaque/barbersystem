package com.projetobeta.barbersystem;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.projetobeta.barbersystem.general.AbreTela;
import com.projetobeta.barbersystem.general.FullScrean;

public class Clientes extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new FullScrean(this);
        setContentView(R.layout.activity_clientes);
        new AbreTela(getSupportFragmentManager(),new com.projetobeta.barbersystem.funcionalidades.clientes.Clientes(),R.id.setTelaClientes);
    }
}
